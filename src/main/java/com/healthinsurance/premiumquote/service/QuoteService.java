package com.healthinsurance.premiumquote.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.healthinsurance.premiumquote.entity.Person;
import com.healthinsurance.premiumquote.repository.QuoteRepository;
import com.healthinsurance.premiumquote.utils.Constants;

/**
 * The Class QuoteService.
 */
@Service
public class QuoteService {

	/** The repository. */
	@Autowired
	private QuoteRepository repository;
	
	/** The person. */
	private Person person;
	
	/** The base price. */
	private long basePrice = Constants.BASE_PRICE;

	/** The log. */
	private final Logger LOG = LoggerFactory.getLogger(QuoteService.class);

	/**
	 * Gets the person.
	 *
	 * @return the person
	 */
	public Person getPerson() {
		return person;
	}

	/**
	 * Sets the person.
	 *
	 * @param person the new person
	 */
	public void setPerson(final Person person) {
		this.person = person;
	}

	/**
	 * Gets the base price.
	 *
	 * @return the base price
	 */
	public long getBasePrice() {
		return basePrice;
	}

	/**
	 * Sets the base price.
	 *
	 * @param basePrice the new base price
	 */
	public void setBasePrice(final long basePrice) {
		this.basePrice = basePrice;
	}

	/**
	 * Gets the quote.
	 *
	 * @param person the person
	 * @return the quote
	 */
	public long getQuote(final Person person) {
		repository.save(person);
		this.setPerson(person);
		return generateQuote();
	}

	/**
	 * Generate quote.
	 *
	 * @return the long
	 */
	public long generateQuote() {
		long quote;
		quote = this.getBasePrice();
		quote = ageFactor();
		quote = genderFactor(quote);
		quote = hypertensionFactor(quote);
		quote = bloodPressureFactor(quote);
		quote = bloodSugarFactor(quote);
		quote = overWeightFactor(quote);
		quote = alcoholicFactor(quote);
		quote = smokerFactor(quote);
		quote = drugAddictFactor(quote);
		quote = excerciseFactor(quote);
		return quote;

	}

	/**
	 * Age factor.
	 *
	 * @return the long
	 */
	private long ageFactor() {
		int ageFactorPercent = 40;
		long quote = this.getBasePrice();
		final int age = this.person.getAge();
		return calculateQuoteByAge(age, quote, ageFactorPercent);
	}

	/**
	 * Calculate quote by age.
	 *
	 * @param age the age
	 * @param quote the quote
	 * @param ageFactorPercent the age factor percent
	 * @return the long
	 */
	private long calculateQuoteByAge(int age, long quote, int ageFactorPercent) {
		if (age >= 18) {
			quote = Math.round(quote + quote * 0.1);
		}
		if (age >= 25) {
			quote = Math.round(quote + quote * 0.1);
		}
		if (age >= 30) {
			quote = Math.round(quote + quote * 0.1);
		}
		if (age >= 35) {
			quote = Math.round(quote + quote * 0.1);
		}
		if (age >= 40) {
			int ageCounter = 40;
			ageFactorPercent = 40;
			while (age > ageCounter) {
				ageCounter += 5;
				quote = Math.round(quote + quote * 0.2);
			}
		}
		if(LOG.isDebugEnabled()){
			LOG.debug("Quote :: "+quote);
		}
		return quote;
	}

	/**
	 * Gender factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long genderFactor(final long quote) {
		if (this.person.getGender() == Constants.MALE) {
			return Math.round(quote + quote * 0.02);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Hypertension factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long hypertensionFactor(final long quote) {
		if (this.person.isHasHypertension()) {
			return Math.round(quote + quote * 0.01);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Blood pressure factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long bloodPressureFactor(final long quote) {
		if (this.person.isHasBP()) {
			return Math.round(quote + quote * 0.01);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Blood sugar factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long bloodSugarFactor(final long quote) {
		if (this.person.isHasBloodSugar()) {
			return Math.round(quote + quote * 0.01);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Over weight factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long overWeightFactor(final long quote) {
		if (this.person.isOverWeight()) {
			return Math.round(quote + quote * 0.01);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Smoker factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long smokerFactor(final long quote) {
		if (this.person.isSmoker()) {
			return Math.round(quote + quote * 0.03);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Alcoholic factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long alcoholicFactor(final long quote) {
		if (this.person.isAlcholic()) {
			return Math.round(quote + quote * 0.03);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Drug addict factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long drugAddictFactor(final long quote) {
		if (this.person.isDrugAddict()) {
			return Math.round(quote + quote * 0.03);
		} else {
			return Math.round(quote);
		}
	}

	/**
	 * Excercise factor.
	 *
	 * @param quote the quote
	 * @return the long
	 */
	private long excerciseFactor(final long quote) {
		if (this.person.isExcerciseFlag()) {
			return Math.round(quote - quote * 0.03);
		} else {
			return Math.round(quote);
		}
	}
}
