package com.healthinsurance.premiumquote.utils;

/**
 * The Interface Constants.
 */
public interface Constants {

	/** The empty string. */
	String EMPTY_STRING = "";

	/** The male. */
	Character MALE = 'M';

	/** The female. */
	Character FEMALE = 'F';

	/** The base price. */
	long BASE_PRICE = 5000;

}
