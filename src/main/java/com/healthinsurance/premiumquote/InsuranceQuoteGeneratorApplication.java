package com.healthinsurance.premiumquote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class InsuranceQuoteGeneratorApplication.
 */
@SpringBootApplication
public class InsuranceQuoteGeneratorApplication {

	/**
	 * The main method.
	 *
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(InsuranceQuoteGeneratorApplication.class, args);
	}
}
