package com.healthinsurance.premiumquote;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.healthinsurance.premiumquote.entity.Person;
import com.healthinsurance.premiumquote.service.QuoteService;
import com.healthinsurance.premiumquote.utils.Constants;

/**
 * The Class QuoteController.
 */
@RestController
public class QuoteController {

	/** The logger. */
	private final Logger LOG = LoggerFactory.getLogger(QuoteController.class);

	/** The service. */
	@Autowired
	QuoteService service;

	/**
	 * Generate insurance quote.
	 *
	 * @param person the person
	 * @return the string
	 */
	@RequestMapping(value = "/getHealthInsurancePremiumQuote", 
					method = RequestMethod.POST, 
					consumes = MediaType.APPLICATION_JSON_VALUE, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public String generateInsuranceQuote(@RequestBody Person person) {
		String calculatedPremiumQuote = Constants.EMPTY_STRING;
		try {
			final String prefix = person.getGender() == Constants.MALE ? "Mr."
					: person.getGender() == Constants.FEMALE ? "Ms." : Constants.EMPTY_STRING;

			calculatedPremiumQuote = "Health Insurance Premium for " + prefix + "" + person.getLastName() + ": Rs."
					+ Math.round(service.getQuote(person));
		} catch (final Exception ex) {
			LOG.error("Exception occured due to :: " + ex.getStackTrace());
		}
		return calculatedPremiumQuote;
	}

}
